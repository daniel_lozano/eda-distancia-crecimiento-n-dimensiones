#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <fstream>

using namespace std;

typedef float real;
typedef float metric;
typedef vector<real> v_real;

size_t  size_vectors = 1000,
        size_distances = 10000,
        size_divisions = 100;

vector<v_real *> vectors(size_vectors);
vector<metric> distances(size_distances);

template<typename T>
void print(T & data, ostream & out)
{
    out << data << "\t";
}

template<typename T>
void print2(T & container, ostream & out)
{
    for(auto & elem : container)
        out << elem << ",";
    out << endl;
}

real distance(v_real * v1, v_real * v2)
{
    real dis = 0;

    for(size_t i=0 ; i<v1->size() ; ++i )
        dis += ((*v1)[i] - (*v2)[i]) * ((*v1)[i] - (*v2)[i]);
    return sqrt(dis);
}

void generate(size_t dim)
{
    //random_device rd;
    mt19937 random(random_device{}());
    uniform_real_distribution<> float_distribution(0, 5);
    uniform_int_distribution<> int_distribution(0, size_vectors-1);

    for(auto & elem : vectors)
    {
        elem = new v_real(dim);
        for(auto & elem2 : *elem)
            elem2   = float_distribution(random);
    }

    int random1, random2;
    for(auto & elem : distances)
    {
        random1 = int_distribution(random);        
        do
        {
            random2 = int_distribution(random);
        }while(random1 == random2);
        elem = distance(vectors[random1], vectors[random2]);
    }
    sort(distances.begin(), distances.end());

    ofstream stream;

    stream.open("output"+ to_string(dim) + ".txt");
    for(size_t i = 0 ; i < size_distances ; i=i+size_divisions)
        print(distances[i], cout);

    stream.close();
}

int main()
{
    vector<int> dimensions = {3, 5, 10, 30, 50, 100, 300, 500};

    for(auto & elem : dimensions)
        generate(elem);

    return 0;
}

